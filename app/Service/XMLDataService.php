<?php

namespace App\Service;

use Illuminate\Support\Facades\Storage;

class XMLDataService
{
    public $xmlFile;
    public array $products;
    public array $storeProducts;
    public  $categories;
    private $service;

    public function __construct(XMLToArrayService $service)
    {
        $this->service = $service;
    }


    public function getFile($data)
    {
        $this->xmlFile = file_get_contents($data['xml_url']);
        return $this->xmlFile;
    }

    public function getListProduct($file) : array
    {
        if (is_string($file)) {
            $xml = simplexml_load_string($file);
            $simpleProducts = $xml->shop->offers->offer;
            $this->products = $this->service->XMLToArrayProducts($simpleProducts);
            return $this->products;

        }
    }

    public  function  getCategories($file) : array
    {
        if (is_string($file)) {
            $xml = simplexml_load_string($file);
            $simpleCategories = $xml->shop->categories->category;
            $this->categories = $this->service->XMLToArrayCategories($simpleCategories);
            return $this->categories;
        }
    }
    public function getDataStore($products, $categories) {
         foreach ($products as $product) {
             $sub_category = $categories[$product['category_id']]['name'];
             $parentId = $categories[$product['category_id']]['parent_id'];
             $category = $categories[$parentId]['name'];
             $product['sub_category'] = $sub_category;
             $product['category'] = $category;
             unset($product['category_id']);
             $this->storeProducts[] = $product;
         }
         return $this->storeProducts;
    }


}
