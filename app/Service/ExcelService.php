<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExcelService
{
    protected $spreadsheet;

    public function __construct(Spreadsheet $spreadsheet)
    {
        $this->spreadsheet = $spreadsheet;
    }

    public function arrayToXlsx($data)
    {
        $sheet = $this->spreadsheet->getActiveSheet();
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->mergeCells('A1:I1');
        $sheet->setCellValue('A1', 'Список товаров');
        $sheet->setCellValue('A2', 'Название');
        $sheet->setCellValue('B2', 'Категория');
        $sheet->setCellValue('C2', 'Подкатегория');
        $sheet->setCellValue('D2', 'Доступность');
        $sheet->setCellValue('E2', 'ID');
        $sheet->setCellValue('F2', 'Цена');
        $sheet->setCellValue('G2', 'Старая цена');
        $sheet->setCellValue('H2', 'Ссылка на товар');
        $sheet->setCellValue('I2', 'Ссылка на изображение');

        $highestRow = $sheet->getHighestRow() + 1;

        foreach ($data as $key=>$value) {
            $sheet->setCellValue("A$highestRow", "{$value->name}");
            $sheet->setCellValue("B$highestRow", "{$value->category}");
            $sheet->setCellValue("C$highestRow", "{$value->sub_category}");
            $sheet->setCellValue("D$highestRow", "{$value->available}");
            $sheet->setCellValue("E$highestRow", "{$value->product_id}");
            $sheet->setCellValue("F$highestRow", "{$value->price}");
            $sheet->setCellValue("G$highestRow", "{$value->old_price}");
            $sheet->setCellValue("H$highestRow", "{$value->url}");
            $sheet->setCellValue("I$highestRow", "{$value->picture}");

            $highestRow ++;
        }

        $dt = date('h-i-s');

        $writer = new Xlsx($this->spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename='file'$dt.xlsx");
        $writer->save('php://output');
    }


}
