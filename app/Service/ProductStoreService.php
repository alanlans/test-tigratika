<?php

namespace App\Service;

use Illuminate\Support\Facades\DB;

class ProductStoreService
{

    public function storeProduct($data)
    {
        try {
            DB::table('products')->insert($data);
        } catch (\Exception $e) {
            echo 'что-то пошло не так' . $e->getMessage(), "\n";
        }
    }
}
