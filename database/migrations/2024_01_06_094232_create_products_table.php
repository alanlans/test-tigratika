<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('product_id')->unique();
            $table->string('name');
            $table->string('url');
            $table->string('price');
            $table->string('old_price')->nullable();
            $table->string('currency_id');
            $table->string('category');
            $table->string('sub_category');
            $table->string('picture');
            $table->string('available');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
